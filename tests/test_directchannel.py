import os
import logging
import pytest

from pydirectchannel import DirectChannel

DC_ENV = os.getenv('DC_ENV')
DC_APP = os.getenv('DC_APP')
DC_TOKEN = os.getenv('DC_TOKEN')
DC_USER = os.getenv('DC_USER')

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger()

@pytest.fixture()
def directchannel():
    if not DC_ENV or not DC_APP or not DC_TOKEN or not DC_USER:
        raise Exception("You must specify DC_ENV, DC_APP, DC_TOKEN, DC_USER")

    dc = DirectChannel(
        env=DC_ENV, 
        application=DC_APP,
        token=DC_TOKEN,
        user=DC_USER
    )
    yield dc


class TestDirectChannel:
    def test_get_wsc_table(self, directchannel):    
        response = directchannel.wsc_table("generi")
        assert response.get('data', False) != False
        assert isinstance(response['data'], list)

    